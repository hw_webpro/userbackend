import { IsNotEmpty, MinLength, Matches, IsInt } from 'class-validator';
import { min } from 'rxjs';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  id: number;

  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsInt()
  @IsNotEmpty()
  price: string;
}
